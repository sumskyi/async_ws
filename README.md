#Async web service

## gems:
* rack
* thin
* eventmachine
* em-mysql


Write a simple asynchronous web service which run the SQL query:

    SELECT SLEEP(0.1);

use ab (Apache Bench) to benchmark your application:

    ab -n 1000 -c 100 localhost:4242/

You should optimize your application to reach 1000#/sec.


## links:
* https://github.com/macournoyer/thin/blob/master/example/async_app.ru
* https://github.com/mperham/evented/blob/master/thin/thumbnailer.rb


## how to run
    thin -p 4242 -R config.ru start

