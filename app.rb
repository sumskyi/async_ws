require 'em-mysqlplus'

DbConfig = {
  :host     => 'localhost',
  :username => 'root',
  :database => 'test',
  :port     => 3308,
}

class DeferrableBody
  include EventMachine::Deferrable

  def call(body)
    body.each do |chunk|
      @body_callback.call(chunk)
    end
  end

  def each &blk
    @body_callback = blk
  end
end

class App
  AsyncResponse = [-1, {}, []].freeze

  def call(env)
    body = DeferrableBody.new
    EventMachine.next_tick { env['async.callback'].call [200, {'Content-Type' => 'text/plain'}, body] }

    error_callback = ->(res) do
      body.call [ 'err' ]
      body.fail
    end
    query_callback = ->(res) do
      body.call [res.all_hashes]
      body.succeed
    end

    EventMachine.next_tick do
      query = connection_pool.with do |connection|
        connection.query("SELECT SLEEP(0.1);")
      end

      query.errback(&error_callback)
      query.callback(&query_callback)
    end

    AsyncResponse
  end

private

  def connection_pool
    @_connection_pool ||= NaiveConnectionPool.new(:size => 150) { EventMachine::MySQL.new(DbConfig) }
  end

end

# this implements API similar to mperham's connection_pool
class NaiveConnectionPool
  attr_accessor :pool

  def initialize(opts)
    @pool = []
    size = opts.fetch(:size)
    size.times { @pool << yield }
  end

  def with
    connection = @pool.shift
    yield connection
  ensure
    @pool << connection
  end
end
